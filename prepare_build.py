"""
CARPI OBD II DAEMON
(C) 2018, Raphael "rGunti" Guntersweiler
Licensed under MIT
"""
import os
from datetime import datetime
from time import time


def read_version():
    with open('VERSION.txt', 'r') as f:
        return f.read().strip()


def read_build_version():
    with open('VERSION.autobuild.txt', 'r') as f:
        return f.read().strip()


def get_version():
    build_time = datetime.fromtimestamp(time()).strftime('%y%m%d%H%M%S')
    base_ver = read_version()

    if 'CI_BUILD_REF_NAME' in os.environ:
        branch = os.environ['CI_BUILD_REF_NAME']
        if branch == "develop":
            # Develop Build
            return '{}.dev{}'.format(base_ver, build_time)
        elif branch.startswith('release/'):
            # Release Candidate Build
            base_ver = branch.split('/')[1]
            return "{}rc{}".format(base_ver, build_time)
        else:
            # Release Build
            return os.environ['CI_BUILD_REF_NAME']
    else:
        return '{}'.format(base_ver)


base_ver = get_version()
with open('VERSION.autobuild.txt', 'w') as build_file:
    build_file.write(base_ver)
